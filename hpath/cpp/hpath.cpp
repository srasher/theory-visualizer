#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <cstring>

using namespace std;

int counter(char * infile)
{
  FILE *fp;
    fp = fopen(infile, "r");
 
    if(fp == NULL)
    {
      cout << "Input file not readable.\n";
      exit(1);
    } //if file open error

  int count=0;
  char line[10000];
  fgets(line, 9999, fp);
  strtok(line, ",");
  //count number of commas
  for(count=0; strtok(NULL, ",") != NULL; count++)

  fclose(fp);
 
  return count;
}

void print(int count)
{
  //count is number of commas, so it is number of vertices minus one.
  //print head - collection of (count+1) vertices.
  cout << "path(X0"; 
  for(int i=1; i<=count; i++)
    cout << ",X" << i;
  cout << ") :- ";
  //print edges of path - connection of (count) edges.
  cout << "n(X0,X1)";
  for(int i=1; i<count; i++)
    cout << ", n(X" << i << ",X" << i+1 << ")";
  //make sure every vertex is distinct
  for(int i=0; i<=count; i++)
    for(int j=i+1; j<=count ; j++)
      cout << ", X" << i << "!=X" << j;
  cout << "." << endl;
} //print

int main(int argc, char * argv[])
{
  print(counter(argv[1]));
  return 0;
} //main
