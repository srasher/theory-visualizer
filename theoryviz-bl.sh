#!/bin/bash

ARGC=$#
FILENAME="${1}"
#echo "Number of args: $ARGC"

i=2
if [ ${i} -le ${ARGC} ]; then
  ARGS=""
  while [ ${i} -le ${ARGC} ]; do
    ARGS="${ARGS} ${!i} ${FILENAME}/${FILENAME}$((i-1)).dlv"
    i=$((i+1))
  done
#  echo "${ARGS}"
fi
./graphcleaner-bl ${ARGS}

cat "${FILENAME}"/"${FILENAME}"*.dlv >  "${FILENAME}"/"${FILENAME}".dlv
chmod 755 "${FILENAME}"/"${FILENAME}".dlv

python awf-bl.py edge,vertex  "${FILENAME}"/"${FILENAME}".dlv

./worlds2dot-bl "${FILENAME}"/"${FILENAME}"_aw.asp "${FILENAME}"/stylesheet.csv > "${FILENAME}"/result.dot

dot "${FILENAME}"/result.dot -Tsvg -o "${FILENAME}"/result.svg
