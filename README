We run the theory visualizer as follows:
theoryviz.sh [filename] [graph1] [graph2] …
where [filename] is the name of the problem you are interested in (hpath, isomorphism, etc.) There must be a corresponding directory for the problem you pick.
  Every subsequent input argument should be the name of a graph. The number of graphs to input depends on the problem. For example, hcycle expects one graph to check for Hamiltonian cycles but homomorphism expects graphs g1,g2 to check for a mapping from g1 to g2. 
  Graph files should take the form of a set of edges in datalog format, ie 
    e(a,b).
    e(b,c).
    e(b,d).
  The variable names can be any valid Datalog variable names and the name of the relation is irrelevant.
  The script takes the graph input files through graphcleaner, a C++ executable which renames the input graphs to g1, g2,....for uniformity and generality of code.
What goes in the directory named [filename]:
  [filename]_code.dlv which contains the Datalog code necessary to solve your problem. Assume your input graph is g1 (and g2 if you use a second input, and so on). In the problems for this project, each of these code files utilizes answer set programming to generate an “in” set of vertices or edges.
  [filename]_grapher.dlv which contains the Datalog code that lends styles to the relations in your solution. The grapher code should focus on categorizing all vertices and edges used in the solution and placing them appropriately in the following two relations:
   vertex(X,S) where X is the vertex name and S is a keyword for the style you would like to apply to X.
    edge(X,S,Y) which represents an edge that goes from vertex X to vertex Y and has the style keyword S.
  stylesheet.csv which is made up of comma-separated pairs of keywords and corresponding styles.
    The top line of this file should read
    global,[rules], 
    where [rules] are the rules you’d like to apply to the entire graph. For instance, if one was interested in a particular orientation and node separation, one may write:
    global,rankdir=TB nodesep=0.3,
    The second line of this file should read
    X,Y,
    where X and Y are vertex names in the graph. If instead of viewing multiple worlds lined up you’d like to view them in a “square” layout, use this to choose the vertices between which the worlds will be connected (via invisible edges). If you don’t mind a linear layout or don’t have multiple worlds, simply write
    none,none,
    for the second line.
    After the second line, you may define your rules. Write your keyword that you chose in the grapher and follow it by the rules you would like to apply to the nodes or edges that you labeled with that keyword. Here are some examples:
    red,node[shape=circle style=filled fillcolor=red],
    "n",edge[arrowhead=none penwidth=3],
    There are some special predefined rules you can use as well:
      To define the style for the edges that connect worlds, use the keyword “split”. By default, we use:
      split,edge[style=invisible arrowhead=none],
      Subgraphs are also rudimentarily supported. To define rules of subgraphs, write
      subgraph,[rules],
      To denote which nodes you’d like to place in which subgraph, precede the rules of those nodes with $k, where k=1,2,... This looks like this:
      v1,$1node[shape=circle fillcolor="#FF9999"],
      v2,$2node[shape=circle fillcolor="#99FF99"],
      Here all the vertices in v1 are in one subgraph while the vertices in v2 are in another.

IMPORTANT NOTE: Do not use commas between styles if your node or edge needs multiple. Separate them by spaces instead. Also make sure to end every rule with a comma. For example, a line like this:
red,node[shape=circle,style=filled,fillcolor=red]
violates both of these rules and should be written as follows:
red,node[shape=circle style=filled fillcolor=red],

Assuming that your directory is properly assembled, the script puts the input graphs, code file, and grapher file into one large dlv file (named [filename].dlv) and gives it to the “Save All Worlds” script awf.py, which produces a nice .asp file of all the worlds generated. Then the C++ executable worlds2dot reads in this .asp file and the stylesheet and matches the keywords in the relations to the keywords in the stylesheet to produce a dot file with the proper vertices, edges, and styles. Finally, the script runs the dot executable on the dot file to produce an svg output.
Your visualized problem will be named result.svg.
