#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <cstring>
#include <cmath>

using namespace std;

class Vertex
{
public:
  string name,style,raw;
  int world;
  void vbuild()
  {
    char * line = strdup(raw.c_str());
    string w = strtok(line, ",");
    world = atoi(strdup(w.c_str()));
    name = strtok(NULL, ",");
    name += w;
    style = strtok(NULL, ",");
//cout << name << " " << style << endl;
  } //vbuild
}; //end class Vertex

class Edge
{
public:
  string src,dest,style,raw;

  void ebuild()
  {
    char * line = strdup(raw.c_str());
    string world = strtok(line, ",");
    src = strtok(NULL, ",");
    src = src + world;
    style = strtok(NULL,",");
    dest = strtok(NULL,",");
    dest = dest + world;
//cout << style << " " << src << " " << dest << endl;
  } //ebuild
}; //end class Edge

class Graph
{
public:
  vector<Vertex> vertices;
  vector<Edge> edges;
  int worlds;
  
  void setworld()
  {
    for (unsigned int i=0; i<vertices.size(); i++)
      worlds = atoi(vertices[i].name.substr(1).c_str()) + 1;
  } //setworld
}; //end class Graph

char *strdup (const char *s) {
    char * d = (char *)malloc(strlen(s) + 1);   // Space for length plus null
    if (d == NULL) return NULL;          // No memory
    strcpy(d,s);                        // Copy the characters
    return d;                            // Return the new string
}

class Style
{
public:
  string head, rule;
}; //end class Style
  
string ltrim(string x)
{
  unsigned int i=0;
  for(i=0; i<x.length() && (x[i]==' ' || x[i] =='{' || x[i]==',');i++);
  return x.substr(i,x.length());
} //ltrim

int findlabel(string rule)
{
  //we want to see if the rule contains a "label" rule.
  for(unsigned int i=0; i<rule.length()-7; i++)
    if(rule[i]=='l' && rule[i+1]=='a' && rule[i+2]=='b' && rule[i+3]=='e' && rule[i+4]=='l')
      return 1;
  return 0;
} //findlabel

string vname(string raw)
{
  //we have a string of the form w%x, where w is a string w/o integers
  // and x is a string of integers. Want w only.
  unsigned int i;
  for (i=0; i<raw.length(); i++)
    if(isdigit(raw[i])) break;
  return raw.substr(0,i);
} //vname

void dlv(vector<Graph>* graphs, char * infile)
{
  FILE *fp;
    fp = fopen(infile, "r");
 
    if(fp == NULL)
    {
      cout << "Input file not readable.\n";
      exit(1);
    } //if file open error

  char line[1000];
  fgets(line, 999, fp);
  Graph graph;
  graph.worlds=0;
  while(!feof(fp))
  {
    char * flag = strtok(line, "(");
    char * flag2 = strtok(NULL, ")");
    while(flag != NULL && flag2!=NULL)
    {
      string style = flag, data = flag2;
      style = ltrim(style);
//cout << style << "|"  << data << endl;
      if(style=="edge")
      {
        Edge edge;
        edge.raw = data;
        graph.edges.push_back(edge);
      }
      if(style=="vertex")
      {
        Vertex vertex;
        vertex.raw = data;
        graph.vertices.push_back(vertex);
      }
      flag = strtok(NULL, "(");
      if(flag!=NULL) flag2 = strtok(NULL, ")");
    } //get all vertices 
    //clean up the raw data now that strtok is free
    for(unsigned int i=0; i<graph.edges.size(); i++)
      graph.edges[i].ebuild();
    for(unsigned int i=0; i<graph.vertices.size(); i++)
      graph.vertices[i].vbuild();
    fgets(line, 999, fp);
  } //for each graph
  graphs->push_back(graph);

  fclose(fp);
} //dlv

void csv(vector<Style>* styles, char * infile)
{
  FILE *fp;
    fp = fopen(infile, "r");
 
    if(fp == NULL)
    {
      cout << "Stylesheet file not readable.\n";
      exit(1);
    } //if file open error

  char line[1000];
  fgets(line, 999, fp);
  while(!feof(fp))
  {
    Style style;
    style.head = strtok(line,",");
    style.rule = strtok(NULL,",");
    styles->push_back(style);
    fgets(line, 999, fp);
  }

}//csv

void dot(vector<Graph>* graphs, vector<Style>* styles)
{
  cout << "digraph{" << endl;
  cout << styles->at(0).rule << endl; //global rule is kept here
  string src = styles->at(1).head, dest = styles->at(1).rule,substyle,worldstyle;
  for(unsigned int k=0; k<styles->size();k++)
  {
    Style style = styles->at(k);
    if(style.head=="split") worldstyle = style.rule;
    if(style.head=="subgraph") substyle = style.rule;
  } //get other important styles
  for(unsigned int i=0; i < graphs->size(); i++)
  {
    Graph graph = graphs->at(i);
    graph.setworld();
    int worldsplit = ceil(sqrt(graph.worlds)), subgraph=0, labelled=0;
//cout << graph.worlds << " " << worldsplit << endl;
    string active;
    for(unsigned int j=0; j< graph.vertices.size(); j++)
    {
      Vertex vertex = graph.vertices[j];
      for(unsigned int k=0; k<styles->size();k++)
      {
        Style style = styles->at(k);
        if(vertex.style == style.head)
        {
          labelled = findlabel(style.rule);
          if(style.rule != active)
          {
            if(subgraph) cout << '}' << endl;
            subgraph=0; //if we're changing styles it's time to end the subgraph.
            if(style.rule[0]=='$')
            {
              cout << "subgraph cluster" << vertex.world << style.rule[1]  
                   << "{\n" << substyle << '\n' << style.rule.substr(2) << endl;
              subgraph=1;
            }
            else
              cout << style.rule << endl;
          }
          active = style.rule;
        } 
      }//find applicable styles
        cout << '"' << vertex.name << '"' ;
        if(!labelled) cout << "[label=" << vname(vertex.name) << "]";
        cout << '\n';
    } //print vertices first
    if(subgraph) cout << '}' << endl;
    subgraph=0; //if we're looking at edges now it's time to end the subgraph.
    for(unsigned int j=0; j< graph.edges.size(); j++)
    {
      Edge edge = graph.edges[j];
      for(unsigned int k=0; k<styles->size();k++)
      {
        Style style = styles->at(k);
        if(edge.style == style.head)
        {
          if(style.rule != active)
            cout << style.rule << endl;
          active = style.rule;
        }
      } //find applicable styles
      cout << '"' << edge.src << "\" -> \"" << edge.dest << '"' << endl;
    } //followed by edges
    cout << worldstyle << endl;
    if(src!="none" && dest!="none")
    {
      for(int j=worldsplit; j<graph.worlds; j++)
      {
        cout << '"' << src << j-worldsplit << "\" -> \"" << dest << j 
             << '"' << endl;
      } //Draw invisible edges to connect worlds in same equivalence class
    } //only worldsplit on command
  }
  cout << "}";
} //dot

int main(int argc, char * argv[])
{
  vector<Graph> graphs;
  vector<Style> styles;
  dlv(&graphs, argv[1]); //read DLV input
  csv(&styles, argv[2]); //read CSV stylesheet input
  dot(&graphs, &styles); //print DOT output
  return 0;
} //main
