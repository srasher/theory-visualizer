#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <cstring>
#include <cmath>

using namespace std;

/* 
   Expected running of this program is:
     graphcleaner dirty1 clean1 dirty2 clean2 ...
   where each dirty graph comes with a clean one to be written to.
   We expect at least one dirty/clean pair.
*/ 

class Vertex
{
public:
  string name,style,raw;
  int world;
  void vbuild()
  {
    char * line = strdup(raw.c_str());
    string w = strtok(line, ",");
    world = atoi(strdup(w.c_str()));
    name = strtok(NULL, ",");
    name += "_";
    name = name + w;
    style = strtok(NULL, ",");
//cout << name << " " << style << endl;
  } //vbuild
}; //end class Vertex

class Edge
{
public:
  string src,dest,style,raw;

  void ebuild()
  {
    char * line = strdup(raw.c_str());
    string world = strtok(line, ",");
    src = strtok(NULL, ",");
    src += "_";
    src = src + world;
    style = strtok(NULL,",");
    dest = strtok(NULL,",");
    dest += "_";
    dest = dest + world;
//cout << style << " " << src << " " << dest << endl;
  } //ebuild
}; //end class Edge

class Graph
{
public:
  vector<Vertex> vertices;
  vector<Edge> edges;
  int worlds;

  void setworld()
  {
    for (unsigned int i=0; i<vertices.size(); i++)
      worlds = atoi(vertices[i].name.substr(1).c_str()) + 1;
  } //setworld
}; //end class Graph

char *strdup (const char *s) {
    char * d = (char *)malloc(strlen(s) + 1);   // Space for length plus null
    if (d == NULL) return NULL;          // No memory
    strcpy(d,s);                        // Copy the characters
    return d;                            // Return the new string
}

string convertInt(int number)
{
    if (number == 0)
        return "0";
    string temp="";
    string returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (unsigned int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}

void read(vector<Edge>* edges, char * filename, int graph)
{
  FILE *fp;
    fp = fopen(filename, "r");

    if(fp == NULL)
    {
      cout << "Input file not readable.\n";
      exit(1);
    } //if file open error

  char line[1000];
  fgets(line, 999, fp);
  while(!feof(fp))
  {
    //We assume every line has only a pair of vertices unless it is a comment.
    if(line[0]!='%')
    {
      char * flag = strtok(line, "(");
      flag = strtok(NULL, ",");
      string source = flag;
      flag = strtok(NULL, ")");
      string dest = flag;
      Edge edge;
      edge.src = source;
      edge.src += "_";
      edge.src += convertInt(graph);
      edge.dest = dest;
      edge.dest += "_";
      edge.dest += convertInt(graph);
      edges->push_back(edge);
    } //as long as the line isn't a comment, get the pair.
    fgets(line, 999, fp);
  } //until end of file

  fclose(fp);
} //read

void write(vector<Edge>* edges, char * filename, int graph)
{
  FILE *fp;
    fp = fopen(filename, "w");

    if(fp == NULL)
    {
      cout << "Output file not writable.\n";
      exit(1);
    } //if file open error
  for(unsigned int i=0; i<edges->size(); i++)
  {
    Edge edge = edges->at(i);
    fprintf(fp,"g%d(%s,%s).\n", graph, edge.src.c_str(), edge.dest.c_str());
  }
  fclose(fp);
} //write

void cleanup(char * dirty, char * clean, int graph)
{
  /*all we want from the dirty graph are its edges. Cleanup means reading these pairs
    from a dirty graph and outputting them properly into a clean graph. */
  vector<Edge> edges;
  read(&edges,dirty, graph);
  write(&edges,clean,graph);   
} //cleanup
 
int main(int argc, char * argv[])
{
  for(int i=1; i<argc; i+=2)
  {
    cleanup(argv[i],argv[i+1],(i/2)+1);
  } //read in each dirty/clean pair.
  return 0;
} //main

