#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <cstring>

using namespace std;

class Vertex
{
public:
  string name,style,raw;
  void vbuild()
  {
    char * line = strdup(raw.c_str());
    name = strtok(line, ",");
    style = strtok(NULL,",");
//cout << name << " " << style << endl;
  } //vbuild
}; //end class Vertex

class Edge
{
public:
  string src,dest,style,raw;

  void ebuild()
  {
    char * line = strdup(raw.c_str());
    src = strtok(line, ",");
    style = strtok(NULL,",");
    dest = strtok(NULL,",");
//cout << style << " " << src << " " << dest << endl;
  } //ebuild
}; //end class Edge

class Graph
{
public:
  vector<Vertex> vertices;
  vector<Edge> edges;
}; //end class Graph

char *strdup (const char *s) {
    char * d = (char *)malloc(strlen(s) + 1);   // Space for length plus null
    if (d == NULL) return NULL;          // No memory
    strcpy(d,s);                        // Copy the characters
    return d;                            // Return the new string
}

class Style
{
public:
  string head, rule;
}; //end class Style
  
string ltrim(string x)
{
  unsigned int i=0;
  for(i=0; i<x.length() && (x[i]==' ' || x[i] =='{' || x[i]==',');i++);
  return x.substr(i,x.length());
} //ltrim

void dlv(vector<Graph>* graphs, char * infile)
{
  FILE *fp;
    fp = fopen(infile, "r");
 
    if(fp == NULL)
    {
      cout << "Input file not readable.\n";
      exit(1);
    } //if file open error

  char line[1000];
  fgets(line, 999, fp);
  while(!feof(fp))
  {
    Graph graph;
    char * flag = strtok(line, "(");
    char * flag2 = strtok(NULL, ")");
    while(flag != NULL && flag2!=NULL)
    {
      string style = flag, data = flag2;
      style = ltrim(style);
//cout << style << "|"  << data << endl;
      if(style=="edge")
      {
        Edge edge;
        edge.raw = data;
        graph.edges.push_back(edge);
      }
      if(style=="vertex")
      {
        Vertex vertex;
        vertex.raw = data;
        graph.vertices.push_back(vertex);
      }
      flag = strtok(NULL, "(");
      if(flag!=NULL) flag2 = strtok(NULL, ")");
    } //get all vertices 
    //clean up the raw data now that strtok is free
    for(unsigned int i=0; i<graph.edges.size(); i++)
      graph.edges[i].ebuild();
    for(unsigned int i=0; i<graph.vertices.size(); i++)
      graph.vertices[i].vbuild();
    graphs->push_back(graph);
    fgets(line, 999, fp);
//cout << endl;
  } //for each path

  fclose(fp);
} //dlv

void csv(vector<Style>* styles, char * infile)
{
  FILE *fp;
    fp = fopen(infile, "r");
 
    if(fp == NULL)
    {
      cout << "Stylesheet file not readable.\n";
      exit(1);
    } //if file open error

  char line[1000];
  fgets(line, 999, fp);
  while(!feof(fp))
  {
    Style style;
    style.head = strtok(line,",");
    style.rule = strtok(NULL,",");
    styles->push_back(style);
    fgets(line, 999, fp);
  }

}//csv

void dot(vector<Graph>* graphs, vector<Style>* styles)
{
  cout << "digraph{" << endl;
  cout << styles->at(0).rule << endl; //global rule is kept here
//int i=0;
  for(unsigned int i=0; i < graphs->size(); i++)
  {
    Graph graph = graphs->at(i);
    string active;
    for(unsigned int j=0; j< graph.vertices.size(); j++)
    {
      Vertex vertex = graph.vertices[j];
      for(unsigned int k=0; k<styles->size();k++)
      {
        Style style = styles->at(k);
        if(vertex.style == style.head)
        {
          if(style.rule != active)
            cout << style.rule << endl;
          active = style.rule;
        } 
      }//find applicable styles
        cout << '"' << vertex.name << i << "\" [label=" << vertex.name << "]" << endl;
    } //print vertices first
    for(unsigned int j=0; j< graph.edges.size(); j++)
    {
      Edge edge = graph.edges[j];
      for(unsigned int k=0; k<styles->size();k++)
      {
        Style style = styles->at(k);
        if(edge.style == style.head)
        {
          if(style.rule != active)
            cout << style.rule << endl;
          active = style.rule;
        }
      } //find applicable styles
      cout << '"' << edge.src << i << "\" -> \"" << edge.dest << i << '"' << endl;
    } //followed by edges
  }
  cout << "}";
} //dot

int main(int argc, char * argv[])
{
  vector<Graph> graphs;
  vector<Style> styles;
  dlv(&graphs, argv[1]); //read DLV input
  csv(&styles, argv[2]); //read CSV stylesheet input
  dot(&graphs, &styles); //print DOT output
  return 0;
} //main
