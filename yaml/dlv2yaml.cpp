#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <cstring>

using namespace std;

class Vertex
{
public:
  string name,group,raw;
  void vbuild()
  {
    char * line = strdup(raw.c_str());
    name = strtok(line, ",");
    group = strtok(NULL,",");
//cout << name << " " << group << endl;
  } //vbuild
}; //end class Vertex

class Edge
{
public:
  string src,dest,label,raw;

  void ebuild()
  {
    char * line = strdup(raw.c_str());
    src = strtok(line, ",");
    label = strtok(NULL,",");
    dest = strtok(NULL,",");
//cout << label << " " << src << " " << dest << endl;
  } //ebuild
}; //end class Edge

class Graph
{
public:
  vector<Vertex> vertices;
  vector<Edge> edges;
}; //end class Graph

char *strdup (const char *s) {
    char * d = (char *)malloc(strlen(s) + 1);   // Space for length plus null
    if (d == NULL) return NULL;          // No memory
    strcpy(d,s);                        // Copy the characters
    return d;                            // Return the new string
}

string ltrim(string x)
{
  unsigned int i=0;
  for(i=0; i<x.length() && (x[i]==' ' || x[i] =='{' || x[i]==',');i++);
  return x.substr(i,x.length());
} //ltrim

void dlv(vector<Graph>* graphs, char * infile)
{
  FILE *fp;
    fp = fopen(infile, "r");
 
    if(fp == NULL)
    {
      cout << "Input file not readable.\n";
      exit(1);
    } //if file open error

  char line[1000];
  fgets(line, 999, fp);
  while(!feof(fp))
  {
    Graph graph;
    char * flag = strtok(line, "(");
    char * flag2 = strtok(NULL, ")");
    while(flag != NULL && flag2!=NULL)
    {
      string label = flag, data = flag2;
      label = ltrim(label);
//cout << label << "|"  << data << endl;
      if(label=="edge")
      {
        Edge edge;
        edge.raw = data;
        graph.edges.push_back(edge);
      }
      if(label=="vertex")
      {
        Vertex vertex;
        vertex.raw = data;
        graph.vertices.push_back(vertex);
      }
      flag = strtok(NULL, "(");
      if(flag!=NULL) flag2 = strtok(NULL, ")");
    } //get all vertices 
    //clean up the raw data now that strtok is free
    for(unsigned int i=0; i<graph.edges.size(); i++)
      graph.edges[i].ebuild();
    for(unsigned int i=0; i<graph.vertices.size(); i++)
      graph.vertices[i].vbuild();
    graphs->push_back(graph);
    fgets(line, 999, fp);
//cout << endl;
  } //for each path

  fclose(fp);
} //dlv

void yaml(vector<Graph>* graphs)
{
int i=0;//  for(unsigned int i=0; i < graphs->size(); i++)
  {
    Graph graph = graphs->at(i);
    for(unsigned int j=0; j< graph.vertices.size(); j++)
    {
      Vertex vertex = graph.vertices[j];
      cout << vertex.name << ":\n  group: " << vertex.group << endl;
    }
    for(unsigned int j=0; j< graph.edges.size(); j++)
    {
      Edge edge = graph.edges[j];
      cout << edge.src << "_" << edge.dest << ":\n  label: " << edge.label << "\n  s: "
           << edge.src << "\n  t: " << edge.dest << endl;
    }
  }
} //yaml

int main(int argc, char * argv[])
{
  vector<Graph> graphs;
  dlv(&graphs, argv[1]); //read DLV input
  yaml(&graphs); //print YAML output
  return 0;
} //main
